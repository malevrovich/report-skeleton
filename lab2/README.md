# Лабораторная работа 2

**Название:** "Разработка драйверов блочных устройств"

**Цель работы:** ...

## Описание функциональности драйвера

Реализовать драйвер блочного устройства, создающего виртуальный диск в RAM со следующими разделами:

- Один первичный раздел размером 20Мбайт 
- Один расширенный раздел, содержащий три логических раздела размером 10Мбайт
каждый.

Также каждый записываемый байт должен быть арифметическим
средним от трех предыдущих. 

## Инструкция по сборке

Для сборки использовать

`make`

Для загрузки модуля в ядро:

`sudo insmod lab2.ko`

Для выгрузки модуля:

`sudo rmmod lab2`

Для очистки:

`make clean`


## Инструкция пользователя

1. Просмотр разделов `sudo fdisk -l /dev/vramdisk`

Вывод fdisk:
```
malevrovich@ubuntu:/dev$ sudo fdisk -l /dev/vramdisk
Disk /dev/vramdisk: 50 MiB, 52428800 bytes, 102400 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x36e5756d

Device         Boot Start    End Sectors Size Id Type
/dev/vramdisk1          1  40960   40960  20M 83 Linux
/dev/vramdisk2      40961 102400   61440  30M  5 Extended
/dev/vramdisk5      40962  61440   20479  10M 83 Linux
/dev/vramdisk6      61442  81920   20479  10M 83 Linux
/dev/vramdisk7      81922 102400   20479  10M 83 Linux
```

2. Измерение скорости записи `sudo ./bench.sh`:

```
malevrovich@ubuntu:~/io_labs/lab2$ sudo ./bench.sh
Filling virt disk with random data
9+0 records in
9+0 records out
9437184 bytes (9,4 MB, 9,0 MiB) copied, 0,0905737 s, 104 MB/s
From virt disk to virt disk
9+0 records in
9+0 records out
9437184 bytes (9,4 MB, 9,0 MiB) copied, 0,090972 s, 104 MB/s
From virt disk to phys disk
9+0 records in
9+0 records out
9437184 bytes (9,4 MB, 9,0 MiB) copied, 0,0736253 s, 128 MB/s
```

## Примеры использования

```
malevrovich@ubuntu:/dev$ dd if=/dev/zero of=/dev/vramdisk5 bs=1M count=9
9+0 records in
9+0 records out
9437184 bytes (9,4 MB, 9,0 MiB) copied, 0,194694 s, 48,5 MB/s
malevrovich@ubuntu:/dev$ dd if=/dev/random of=/dev/vramdisk5 bs=1M count=9
9+0 records in
9+0 records out
9437184 bytes (9,4 MB, 9,0 MiB) copied, 0,167004 s, 56,5 MB/s
malevrovich@ubuntu:/dev$ sudo xxd /dev/vramdisk5 | head
00000000: 9d97 8a94 918f 9190 9090 9090 9090 9090  ................
00000010: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000020: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000030: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000040: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000050: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000060: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000070: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000080: 9090 9090 9090 9090 9090 9090 9090 9090  ................
00000090: 9090 9090 9090 9090 9090 9090 9090 9090  ................
```



...
