#include <linux/cdev.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/kdev_t.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/types.h>
#include <linux/uaccess.h>
#include <linux/version.h>

static dev_t first;
static struct cdev c_dev;
static struct class *cl;

#define BUF_SIZE 16

static char device_data[BUF_SIZE];
static int written = 0;
static int written_chars = 0;

static int my_open(struct inode *i, struct file *f) {
  printk(KERN_INFO "Driver: open()\n");
  return 0;
}

static int my_close(struct inode *i, struct file *f) {
  printk(KERN_INFO "Driver: close()\n");
  return 0;
}

static ssize_t my_read(struct file *f, char __user *buf, size_t len,
                       loff_t *off) {
  int to_read, remains;
  int pos;
  int available = written;

  pos = (written % BUF_SIZE + *off) % BUF_SIZE;
  if (written < BUF_SIZE) {
    pos = *off % BUF_SIZE;
  }

  if (available > BUF_SIZE) {
    available = BUF_SIZE;
  }
  available -= *off;

  if (available < 0) {
    available = 0;
  }

  printk(KERN_INFO "Driver: read(), written=%d\n", written);

  if (len > available) {
    len = available;
  }

  to_read = (BUF_SIZE - pos);
  if (to_read > len) {
    to_read = len;
  }

  copy_to_user(buf, device_data + pos, to_read);

  remains = len - to_read;

  if (remains > 0) {
    copy_to_user(buf + to_read, device_data, remains);
  }

  *off = len;

  printk(KERN_INFO "Driver: read returns %ld\n", len);
  return len;
}

static ssize_t m_write(struct file *f, const char __user *buf, size_t len,
                       loff_t *off) {
  int to_write, i, remains;
  int pos = written % BUF_SIZE;

  remains = len;
  while (remains > 0) {
    to_write = BUF_SIZE - pos;
    i = 0;

    if (to_write > remains) {
      to_write = remains;
    }

    written += to_write;
    remains -= to_write;

    if (written >= BUF_SIZE) {
      for (i = pos; i < pos + to_write; i++) {
        printk(KERN_INFO "Overwriting symbol %c", device_data[i]);
      }
    }

    memcpy(device_data + pos, buf, to_write);

    pos = (pos + to_write) % BUF_SIZE;
  }

  *off = len % BUF_SIZE;

  return len;
}

static ssize_t my_write(struct file *f, const char __user *buf, size_t len,
                        loff_t *off) {
  char buffer[BUF_SIZE] = "";
  int tmp_out;
  int i = 0;

  printk(KERN_INFO "Driver: write(), written=%d\n", written);

  written_chars += len;

  tmp_out = written_chars;
  while (tmp_out > 0) {
    printk(KERN_INFO "tmp=%d i=%d written_chars=%d", tmp_out, i, written_chars);
    buffer[BUF_SIZE - i - 1] = '0' + tmp_out % 10;
    printk(KERN_INFO "writing to %p", buffer + BUF_SIZE - i - 1);
    tmp_out = tmp_out / 10;
    i += 1;
  }

  printk(KERN_INFO "writing from %p", buffer + BUF_SIZE - i);
  m_write(f, buffer + BUF_SIZE - i, i, off);

  return len;
}

static struct file_operations mychdev_fops = {.owner = THIS_MODULE,
                                              .open = my_open,
                                              .release = my_close,
                                              .read = my_read,
                                              .write = my_write};

static int __init ch_drv_init(void) {
  printk(KERN_INFO "Hello!\n");
  if (alloc_chrdev_region(&first, 0, 1, "ch_dev") < 0) {
    return -1;
  }
  if ((cl = class_create(THIS_MODULE, "chardrv")) == NULL) {
    unregister_chrdev_region(first, 1);
    return -1;
  }
  if (device_create(cl, NULL, first, NULL, "var5") == NULL) {
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    return -1;
  }
  cdev_init(&c_dev, &mychdev_fops);
  if (cdev_add(&c_dev, first, 1) == -1) {
    device_destroy(cl, first);
    class_destroy(cl);
    unregister_chrdev_region(first, 1);
    return -1;
  }
  return 0;
}

static void __exit ch_drv_exit(void) {
  cdev_del(&c_dev);
  device_destroy(cl, first);
  class_destroy(cl);
  unregister_chrdev_region(first, 1);
  printk(KERN_INFO "Bye!!!\n");
}

module_init(ch_drv_init);
module_exit(ch_drv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Author");
MODULE_DESCRIPTION("The first kernel module");
