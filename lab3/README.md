# Лабораторная работа 3

**Название:** "Разработка драйверов сетевых устройств"

**Цель работы:** "получить знания и навыки разработки драйверов сетевых интерфейсов для операционной системы Linux."

## Описание функциональности драйвера

UВЗ пакеты длиной меньше 70 байт. Вывести длину.

Состояние разбора пакетов необходимо выводить в кольцевой буфер ядра.

## Инструкция по сборке

Для сборки использовать

`make`

Для загрузки модуля в ядро:

`sudo insmod lab3.ko`

Для выгрузки модуля:

`sudo rmmod lab3`

Для очистки:

`make clean`

## Инструкция пользователя

```
make

sudo insmod lab3.ko

echo -n "text" | nc -4u -w1 10.0.2.15 7777

dmesg
```

## Примеры использования

```
[  519.431169] Captured UDP datagram, saddr: 172.28.16.1
[  519.431171] daddr: 10.0.2.15
[  519.431172] Data length: 261. Data:
[  519.431173] |\xba\x81\x80|
[  519.431175] Captured UDP datagram, saddr: 172.28.16.1
[  519.431175] daddr: 10.0.2.15
[  519.431176] Data length: 178. Data:
[  519.431176] |\x8fy\x81\x80|
[  520.309724] Captured UDP datagram, saddr: 172.28.16.1
[  520.310440] daddr: 10.0.2.15
[  520.311280] Data length: 146. Data:
[  520.311282] |F$\x81\x80|
[  520.535289] Captured UDP datagram, saddr: 185.125.190.58
[  520.536141] daddr: 10.0.2.15
[  520.536713] Data length: 48. Data:
[  520.536715] |$|
[  520.537113] Sniffed!
[  524.724377] Captured UDP datagram, saddr: 10.0.2.15
[  524.724381] daddr: 224.0.0.251
[  524.724382] Data length: 45. Data:
[  524.724383] ||
[  524.724384] Sniffed!
```
