# Лабораторная работа 1

**Название:** "Разработка драйверов символьных устройств"

**Цель работы:** получить знания и навыки разработки драйверов символьных устройств для операционной системы Linux

## Описание функциональности драйвера

Создает символьное устройство /dev/var5

При записи текста в файл символьного устройства должно запоминаться количество введенных букв. Последовательность полученных результатов с момента загрузки модуля ядра должна выводиться при чтении файла.

## Инструкция по сборке

```
$ make all
make -C /lib/modules/5.4.0-150-generic/build M="/home/malevrovich/io_labs/lab0" modules
make[1]: Entering directory '/usr/src/linux-headers-5.4.0-150-generic'
  CC [M]  /home/malevrovich/io_labs/lab0/ch_drv.o
  Building modules, stage 2.
  MODPOST 1 modules
  CC [M]  /home/malevrovich/io_labs/lab0/ch_drv.mod.o
  LD [M]  /home/malevrovich/io_labs/lab0/ch_drv.ko
```

## Инструкция пользователя

```
sudo insmod ch_drv.ko
sudo chmod 777 /dev/var5
```

## Примеры использования

```
malevrovich@ubuntu:/dev$ sudo cat var5
4malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
48malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
4812malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
481216malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
48121620malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
4812162024malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
481216202428malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
48121620242832malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
4812162024283236malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
malevrovich@ubuntu:/dev$ sudo cat var5
1216202428323640malevrovich@ubuntu:/dev$ sudo echo -n 1111 >var5
```
