#!/bin/bash

chmod 777 /dev/vramdisk* >/dev/null
touch /dev/a >/dev/null
echo "Filling virt disk with random data"
dd if=/dev/random of=/dev/vramdisk5 bs=1M count=9
echo "From virt disk to virt disk"
dd if=/dev/vramdisk5 of=/dev/vramdisk6 bs=1M count=9
echo "From virt disk to phys disk"
dd if=/dev/vramdisk5 of=/dev/a bs=1M count=9
rm /dev/a